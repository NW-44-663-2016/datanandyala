using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using DataNandyala.Models;

namespace DataNandyala.Controllers
{
    public class StorelocsController : Controller
    {
        private AppDbContext _context;

        public StorelocsController(AppDbContext context)
        {
            _context = context;    
        }

        // GET: Storelocs
        public IActionResult Index()
        {
            return View(_context.StoreLocs.ToList());
        }

        // GET: Storelocs/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Storeloc storeloc = _context.StoreLocs.Single(m => m.StoreID == id);
            if (storeloc == null)
            {
                return HttpNotFound();
            }

            return View(storeloc);
        }

        // GET: Storelocs/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Storelocs/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Storeloc storeloc)
        {
            if (ModelState.IsValid)
            {
                _context.StoreLocs.Add(storeloc);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(storeloc);
        }

        // GET: Storelocs/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Storeloc storeloc = _context.StoreLocs.Single(m => m.StoreID == id);
            if (storeloc == null)
            {
                return HttpNotFound();
            }
            return View(storeloc);
        }

        // POST: Storelocs/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Storeloc storeloc)
        {
            if (ModelState.IsValid)
            {
                _context.Update(storeloc);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(storeloc);
        }

        // GET: Storelocs/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Storeloc storeloc = _context.StoreLocs.Single(m => m.StoreID == id);
            if (storeloc == null)
            {
                return HttpNotFound();
            }

            return View(storeloc);
        }

        // POST: Storelocs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Storeloc storeloc = _context.StoreLocs.Single(m => m.StoreID == id);
            _context.StoreLocs.Remove(storeloc);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
