﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using System.IO;

namespace DataNandyala.Models
{
        public static class AppSeedData
        {
       // private static string appPath;

        public static void Initialize(IServiceProvider serviceProvider, string appPath)
        {
            string relPath = appPath + "//Models//SeedData//";
            var context = serviceProvider.GetService<AppDbContext>();

            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }


             //context.StoreLocs.RemoveRange(context.StoreLocs);
            context.Locations.RemoveRange(context.Locations);
            context.SaveChanges();

            SeedLocationsFromCsv(relPath, context);
           // SeedStoreLocsFromCsv(relPath, context);
        }
             private static void SeedLocationsFromCsv(string relPath, AppDbContext context)
            {
            string source = relPath + "location.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            List<Location> lst = Location.ReadAllFromCSV(source);
            context.Locations.AddRange(lst.ToArray());
            context.SaveChanges();
        }

        //private static void SeedStoreLocsFromCsv(string relPath, AppDbContext context)
        //{
        //    string source = relPath + "location.csv";
        //    if (!File.Exists(source))
        //    {
        //        throw new Exception("Cannot find file " + source);
        //    }
        //    List<Storeloc> lst = Storeloc.ReadAllFromCSV(source);
        //    context.StoreLocs.AddRange(lst.ToArray());
        //    context.SaveChanges();
        //}



        // SeedMoviesFromCsv(relPath, context);
        //if (context.Locations.Any())
        //   {
        //        return;   // DB already seeded
        //   }

        //context.Locations.AddRange(
        //    new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" },
        //new Location() { Latitude = 37.6991993, Longitude = -97.4843859, Place = "Wichita", State = "Kansas", Country = "USA" },
        // new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" },
        //new Location() { Latitude = 45.0993041, Longitude = -93.1007215, Place = "Whte Bear Lake", State = "Minnesota", Country = "USA" },
        //new Location() { Latitude = 17.4126272, Longitude = 78.2676166, Place = "Hyderabad", Country = "India" },
        // new Location() { Latitude = 25.9019385, Longitude = 84.6797775, Place = "Bihar", Country = "India" }
        // );
        //context.SaveChanges();
        //  context.StoreLocs.AddRange(
        //      new Storeloc() { StoreID = 1, StoreName = "axc", Type = "retail", LocationID = 6 },
        //      new Storeloc() { StoreID = 2, StoreName = "ACS", Type = "retail" ,LocationID =1},
        //      new Storeloc() { StoreID = 3, StoreName = "AWS", Type = "retail" , LocationID = 2 },
        //      new Storeloc() { StoreID = 4, StoreName = "ARS", Type = "retail" , LocationID = 3},
        //      new Storeloc() { StoreID = 5, StoreName = "AZS", Type = "retail" , LocationID = 4},
        //      new Storeloc() { StoreID = 6, StoreName = "andj", Type = "retail" , LocationID = 5}


        //);
        //      context.SaveChanges();

    }

}
        





