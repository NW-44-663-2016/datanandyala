﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DataNandyala.Models
{
    public class Storeloc
    {
        [ScaffoldColumn(false)]
        [Required]
        [Key]
        public int StoreID { get; set; }
        [Display(Name = "store name")]
        public string StoreName { get; set; }
        [Display(Name = "Type")]
        public string Type { get; set; }

        public static List<Storeloc> ReadAllFromCSV(string filepath)
        {
            List<Storeloc> lst = File.ReadAllLines(filepath)
                                         .Skip(1)
                                         .Select(v => Storeloc.OneFromCsv(v))
                                         .ToList();
            return lst;
        }

        public static Storeloc OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Storeloc item = new Storeloc();

            int i = 0;
            item.StoreID = Convert.ToInt32(values[i++]);
            item.StoreName = Convert.ToString(values[i++]);
            item.Type = Convert.ToString(values[i++]);
            return item;
        }


    }
}
